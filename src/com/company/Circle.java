package com.company;

public class Circle {
    private Point center;
    private Double radius;

    public Circle(Point center, Double radius) {
        this.center = center;
        this.radius = radius;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }
    public boolean containsPoint(Point p){
        double distance = center.distanceTo(p);
        return distance<radius;

    }
    public void showInfo(){
        System.out.println("Центр окружности(x,y):"+center.getX()+","+center.getY()+". Радиус:"+radius);
    }

}
