package com.company;

import java.util.Arrays;

public class PointList {
    private Point[] pointList = new Point[0];

    public void addPoint(Point p) {
        pointList = Arrays.copyOf(pointList, pointList.length + 1);
        pointList[pointList.length - 1] = p;

    }

    public void showContainsPoints(Circle circle) {
        int pointNumber = 0;
        for (int i = 0; i < pointList.length; i++) {
            Point currentPoint = pointList[i];
            if (circle.containsPoint(currentPoint)) {

                pointNumber = i + 1;
                System.out.println("Точка № " + pointNumber + " (" + currentPoint.getX() + "," + currentPoint.getY() + ") находится в пределах окружности");
            }

            if (pointNumber == 0) {
                System.out.println("Нет точек в пределах окружности");
            }
        }
    }
}


