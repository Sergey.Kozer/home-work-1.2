package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        double x;
        double y;

        Scanner scanner = new Scanner(System.in);
        PointList pointList = new PointList();

        String nextMessage = "Желаете добавить еще? (1-да 2-нет)";
        String errorMessage = "Введено неверное значение. Повторите ввод";

        byte answer;
        do{
            System.out.println("Введите координаты точки");
            x = scanDoubleValue(scanner,"x:",errorMessage);
            y = scanDoubleValue(scanner,"y:",errorMessage);

            pointList.addPoint(new Point(x,y));

            while (true) {
                System.out.println(nextMessage);
                if (scanner.hasNextByte()){
                    answer = scanner.nextByte();
                    if (answer ==1 || answer == 2){
                        System.out.println("Ваш выбор "+answer);
                        break;
                    } else {
                        System.out.println(errorMessage);
                        continue;
                    }
                }
                System.out.println(errorMessage);
                scanner.next();
            }

        } while (answer == 1);

        System.out.println("Введите координаты центра окружности");

        double cx = scanDoubleValue(scanner,"x:",errorMessage);
        double cy = scanDoubleValue(scanner,"y:",errorMessage);
        double radius = scanDoubleValue(scanner,"Введите радиус окружности:",errorMessage);

        Circle circle =new Circle(new Point(cx,cy),radius);
        System.out.println("########################################################");
        circle.showInfo();
        pointList.showContainsPoints(circle);
        System.out.println("########################################################");

    }
      private static double scanDoubleValue(Scanner scanner, String message, String errorMessage){
        System.out.print(message);
        while (true) {
            if (scanner.hasNextDouble()){
                return scanner.nextDouble();
            } else {
                System.out.println(errorMessage);
                System.out.print(message);
                scanner.next();
            }
        }
    }

}
